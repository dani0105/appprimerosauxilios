package com.primerosauxilios.udec.appprimerosauxilios.vista.activities;

import android.content.Intent;
import android.support.test.rule.ActivityTestRule;

import com.primerosauxilios.udec.appprimerosauxilios.logica.Aplicacion;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Caso;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DatabasePAConstantes;

import org.junit.After;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

public class CasoAMostrarActivityTest {


    @ClassRule
    public static ActivityTestRule<CasoAMostrarActivity> mActivityTestRuleCasoAMostrar = new ActivityTestRule<CasoAMostrarActivity>(CasoAMostrarActivity.class);

    private static CasoAMostrarActivity casoAMostrarActivity = null;
    private static String nombreCasoDePrueba;
    static Aplicacion aplicacion;

    @BeforeClass
    public static void setUp() throws Exception {
        nombreCasoDePrueba = "Esguince";
        //TODO Arreglar el problema de que la activity no esta recibiendo el nombre del caso desde el intent que se le esta
        //TODO pasando
//        Intent intent = new Intent();
//        intent.putExtra(DatabasePAConstantes.CASO, nombreCasoDePrueba);
//        mActivityTestRuleCasoAMostrar.getActivity().setIntent(intent);
        casoAMostrarActivity = mActivityTestRuleCasoAMostrar.getActivity();
        aplicacion = Aplicacion.getInstancia(casoAMostrarActivity.getApplicationContext());

    }

    @Test
    public void debeComprobarQueSeHaceElCargadoDelCasoDePruebaSuministrado(){
        casoAMostrarActivity.cargarCaso(nombreCasoDePrueba);
        Assert.assertTrue(casoAMostrarActivity.getTitle().toString().equals(nombreCasoDePrueba));
    }
}

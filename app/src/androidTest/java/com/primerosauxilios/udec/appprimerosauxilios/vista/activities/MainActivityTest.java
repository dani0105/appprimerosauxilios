package com.primerosauxilios.udec.appprimerosauxilios.vista.activities;


import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.support.test.annotation.UiThreadTest;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.v7.app.AlertDialog;
import android.widget.NumberPicker;

import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Aplicacion;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Caso;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DatabasePAConstantes;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.dialogs.DialogCreator;

import junit.framework.Assert;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.fail;

public class MainActivityTest {

    @ClassRule
    public static ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);

    private static MainActivity mActivity = null;
    private static Aplicacion aplicacion;
    private AlertDialog dialogoPrueba;
    private static DialogCreator dialogCreator;
    @BeforeClass
    public static void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();
        aplicacion = Aplicacion.getInstancia(mActivity.getApplicationContext());
        dialogCreator = DialogCreator.getInstancia(mActivity);
    }

    @Test
    public void debenRecuperarseLosNombresDeLosCasosQueCoincidanConLaCadenaEnviada(){
        String cadenaPrueba = "a";
        List<String> casos = aplicacion.getNombresCasos(cadenaPrueba);
        Assert.assertNotNull(casos);
        Assert.assertFalse(casos.isEmpty());
    }

    @Test
    public void debeTraerElCasoDeAcuerdoAlNombreSuministrado(){
        String nombreCaso = "Lumbago";
        Caso caso = aplicacion.getCaso(nombreCaso);
        Assert.assertTrue(caso.getNombre().equals(nombreCaso));
    }


    @Test
    public void debeComprobarQueSeCreaElDialogoDelTamanoDeLetra(){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogoPrueba = dialogCreator.crearDialogoTamañoLetra();
                Assert.assertNotNull(dialogoPrueba);
                //TODO Averiguar la manera de comprobar que el NumberPicker esta dentro del Dialog
                //NumberPicker numberPicker = (NumberPicker) dialogoPrueba.findViewById(R.id.dialogo_tamaño_letra);
                //Assert.assertNotNull(numberPicker);
            }
        });

    }

    @Test
    public void debeComprobarQueSeCreaElDialogoDelMarcoLegal(){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogoPrueba = dialogCreator.crearDialogoMarcoLegal();
                Assert.assertNotNull(dialogoPrueba);
            }
        });

    }

    @Test
    public void debeComprobarQueSeCreaElDialogoDeLosCreditos(){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogoPrueba = dialogCreator.crearDialogoCreditos();
                Assert.assertNotNull(dialogoPrueba);
            }
        });

    }

    @Test
    public void debeComprobarQueSeCreaElDialogoDeLosNumerosDeEmergencia(){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogoPrueba = dialogCreator.crearDialogoNumerosEmergencia();
                Assert.assertNotNull(dialogoPrueba);
            }
        });

    }

    @Test
    public void debeComprobarQueSeCreaLosDialogosDelTriage(){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                dialogoPrueba = dialogCreator.crearDialogoTriage(1);
                Assert.assertNotNull(dialogoPrueba);
                dialogoPrueba = dialogCreator.crearDialogoTriage(2);
                Assert.assertNotNull(dialogoPrueba);
                dialogoPrueba = dialogCreator.crearDialogoTriage(3);
                Assert.assertNotNull(dialogoPrueba);
                dialogoPrueba = dialogCreator.crearDialogoTriage(4);
                Assert.assertNotNull(dialogoPrueba);
                dialogoPrueba = dialogCreator.crearDialogoTriage(5);
                Assert.assertNotNull(dialogoPrueba);
            }
        });

    }

    @Test
    public void debeComprobarQueSePasaALaActivityDeLosIndicesdeCasos(){

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(mActivity, IndiceCasosActivity.class);
                    mActivity.startActivity(intent);
                }
                catch (ActivityNotFoundException ex){
                    fail();
                }
            }
        });
    }

    @Test
    public void debeComprobarQueSePasaALaActivityDelCasoEspecificado(){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    String casoPrueba = "Alergia";
                    Intent intent = new Intent(mActivity, CasoAMostrarActivity.class);
                    intent.putExtra(DatabasePAConstantes.CASO, casoPrueba);
                    mActivity.startActivity(intent);
                }
                catch (ActivityNotFoundException ex){
                    fail();
                }
            }
        });
    }

    @Test
    public void debeComprobarQueSePasaALaActivityDeLasEscalas(){
        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(mActivity, EscalaActivity.class);
                    intent.putExtra(DatabasePAConstantes.ESCALA, DatabasePAConstantes.ESCALA_CONSCIENCIA);
                    mActivity.startActivity(intent);

                    intent.putExtra(DatabasePAConstantes.ESCALA, DatabasePAConstantes.ESCALA_DOLOR);
                    mActivity.startActivity(intent);
                }
                catch (ActivityNotFoundException ex){
                    fail();
                }
            }
        });
    }

    @Test
    public void debeComprobarQueSePasaALaActivityDelMapaDeLosHospitalesCercanos(){

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(mActivity, MapaHospitalesActivity.class);
                    mActivity.startActivity(intent);
                }
                catch (ActivityNotFoundException ex){
                    fail();
                }
            }
        });
    }

    @Test
    public void debeComprobarQueSePasaALaActivityDelTutorial(){

        mActivity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {
                    Intent intent = new Intent(mActivity, TutorialYoutubeActivity.class);
                    mActivity.startActivity(intent);
                }
                catch (ActivityNotFoundException ex){
                    fail();
                }
            }
        });
    }


    @After
    public void tearDown() throws Exception {
        dialogoPrueba = null;

    }

    @AfterClass
    public static void tearDownEverything(){
        mActivity = null;
    }
}
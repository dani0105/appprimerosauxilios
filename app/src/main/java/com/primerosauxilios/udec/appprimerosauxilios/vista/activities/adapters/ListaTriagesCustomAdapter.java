package com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Triage;

import java.util.List;

public class ListaTriagesCustomAdapter extends BaseAdapter implements CustomAdapter{

    private Context context;
    private List<Triage> triages;

    public ListaTriagesCustomAdapter(Context context, List<Triage> triages) {
        this.context = context;
        this.triages = triages;
    }

    @Override
    public int getCount() { return triages.size(); }

    @Override
    public Object getItem(int position) {
        return triages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return triages.get(position).getIdTriage();
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View row;
        row = inflater.inflate(R.layout.filalistatriage_listview, parent, false);
        TextView tvTituloTriage;
        TextView tvTiempoTriage;
        TextView tvNumeroTriage;
        TextView tvDescripcionTriage;

        tvTituloTriage = (TextView) row.findViewById(R.id.tvTituloTriage);
        tvNumeroTriage = (TextView) row.findViewById(R.id.tvNumeroTriage);
        tvTiempoTriage = (TextView) row.findViewById(R.id.tvNumeroTriage);
        tvDescripcionTriage = (TextView) row.findViewById(R.id.tvDescripcionTriage);

        SharedPreferences sharedPreferences =
                context.getSharedPreferences(context.getString(R.string.tamañoLetra),
                        context.MODE_PRIVATE);
        int tamañoLetra = sharedPreferences.getInt(context.getString(R.string.tamañoLetra), 15);

        tvTituloTriage.setTextSize(tamañoLetra);
        tvNumeroTriage.setTextSize(tamañoLetra);
        tvTiempoTriage.setTextSize(tamañoLetra);
        tvDescripcionTriage.setTextSize(tamañoLetra);
        Typeface fontRalewayLight = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");

        tvTituloTriage.setTypeface(fontRalewayLight);
        tvNumeroTriage.setTypeface(fontRalewayLight);
        tvDescripcionTriage.setTypeface(fontRalewayLight);

        if (Build.VERSION.SDK_INT >= 24) {
            tvNumeroTriage.setText(Html.fromHtml(""+triages.get(position).getIdTriage()));
            tvTiempoTriage.setText(Html.fromHtml(""+triages.get(position).getTiempoAtencion()));
            tvDescripcionTriage.setText(Html.fromHtml(""+triages.get(position).getDescripcion()));
        } else {
            tvNumeroTriage.setText(Html.fromHtml(""+triages.get(position).getIdTriage()));
            tvTiempoTriage.setText(Html.fromHtml(""+triages.get(position).getTiempoAtencion()));
            tvDescripcionTriage.setText(Html.fromHtml(""+triages.get(position).getDescripcion()
            ));
        }

        tvTituloTriage.setTextColor(Color.WHITE);
        tvTiempoTriage.setTextColor(Color.WHITE);
        tvNumeroTriage.setTextColor(Color.WHITE);
        tvDescripcionTriage.setTextColor(Color.WHITE);

        switch(triages.get(position).getColorTriage()){
            case "RED":
                row.setBackgroundColor(context.getResources().getColor(R.color.redTriage));

            break;

            case "ORANGE":
                row.setBackgroundColor(context.getResources().getColor(R.color.orangeTriage));
            break;

            case "YELLOW":
                row.setBackgroundColor(context.getResources().getColor(R.color.yellowTriage));
                tvTituloTriage.setTextColor(Color.BLACK);
                tvNumeroTriage.setTextColor(Color.BLACK);
                tvDescripcionTriage.setTextColor(Color.BLACK);
            break;

            case "GREEN":
                row.setBackgroundColor(context.getResources().getColor(R.color.greenTriage));
            break;

            case "BLUE":
                row.setBackgroundColor(context.getResources().getColor(R.color.blueTriage));
            break;

        }

        return row;
    }
}

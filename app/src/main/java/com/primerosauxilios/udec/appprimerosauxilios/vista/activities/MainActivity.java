package com.primerosauxilios.udec.appprimerosauxilios.vista.activities;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.SearchView.OnQueryTextListener;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Aplicacion;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DatabasePAConstantes;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.DrawerListAdapter;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.ListaCasosCustomAdapter;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.dialogs.DialogCreator;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.extras.DrawableManager;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.extras.DrawerItem;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnQueryTextListener, OnItemClickListener {
    private ListaCasosCustomAdapter adapter;
    private Toolbar toolbar;
    private String[] itemsMenuLateral;
    private ArrayList<String> listaCasos;
    private ArrayList<Integer> iconosCasos;
    private DrawableManager drawableManager;
    private ListView lvResultados;
    private SearchView simpleSearchView;
    private DrawerLayout drawerLayout;
    private ActionBarDrawerToggle drawerToggle;
    private AppCompatActivity activity;
    private ListView lvNavigationDrawer;

    @SuppressLint("ResourceAsColor")
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        this.activity = this;
        this.simpleSearchView = (SearchView) findViewById(R.id.simpleSearchView);
        this.simpleSearchView.setOnQueryTextListener(this);
        this.lvResultados = (ListView) findViewById(R.id.listaResultadosPrincipal);
        this.lvResultados.setDividerHeight(0);
        this.lvResultados.setDivider(null);
        this.listaCasos = new ArrayList();
        //this.adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, this.listaCasos);
        this.drawableManager = new DrawableManager(this);
        this.iconosCasos = (ArrayList<Integer>) this.drawableManager.obtenerIdsIconosCasos(this.listaCasos);
        this.adapter = new ListaCasosCustomAdapter(this, this.listaCasos, this.iconosCasos);
        this.lvResultados.setAdapter(this.adapter);
        this.lvResultados.setOnItemClickListener(this);
        //this.lvResultados.setBackgroundResource(R.drawable.health_symbol);
        this.lvResultados.setCacheColorHint(android.R.color.transparent);
        configurarToolbar();
        inicializarMenuLateral();

    }

    private void configurarToolbar(){
        setSupportActionBar(this.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.primeros_auxilios_icon);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    private void inicializarMenuLateral() {
        itemsMenuLateral = getResources().getStringArray(R.array.itemsNavigationDrawer);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        lvNavigationDrawer = (ListView) findViewById(R.id.lvMenuLateral);

        ArrayList<DrawerItem> itemsNavigationDrawer = new ArrayList<DrawerItem>();
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[0], R.drawable.primeros_auxilios_icon));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[1], R.drawable.indicedecasos_icon));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[2], R.drawable.triage_icon));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[3], R.drawable.escaladeconsciencia_icon));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[4], R.drawable.escaladeldolor_icon));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[5], R.drawable.numerosdemergencia_icon));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[6], R.drawable.hospital_map_icon_32));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[7], R.drawable.marcolegal_icon));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[8], R.drawable.creditos_icon));
        itemsNavigationDrawer.add(new DrawerItem(itemsMenuLateral[9], R.drawable.manual_icon));

        this.lvNavigationDrawer.setDividerHeight(0);
        this.lvNavigationDrawer.setDivider(null);

        View navigationDrawerHeader = this.getLayoutInflater().inflate(R.layout.layout_header_navigation_drawer, null, false);
        View navigationDrawerFooter = this.getLayoutInflater().inflate(R.layout.layout_footer_navigation_drawer, null, false);

        TextView tvNavigationDrawerHeader = (TextView) navigationDrawerHeader.findViewById(R.id.tvHeaderNavigationDrawer);
        tvNavigationDrawerHeader.setTextSize(30);
        Typeface fontRalewayLight = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Light.ttf");
        tvNavigationDrawerHeader.setTypeface(fontRalewayLight);

        lvNavigationDrawer.addHeaderView(navigationDrawerHeader);
        lvNavigationDrawer.addFooterView(navigationDrawerFooter);
        lvNavigationDrawer.setAdapter(new DrawerListAdapter(this, itemsNavigationDrawer.toArray()));

        lvNavigationDrawer.setOnItemClickListener(getListenerItemsNavigationDrawer(this));
        drawerToggle = getDrawerToggle(drawerLayout);
    }

    private ActionBarDrawerToggle getDrawerToggle(DrawerLayout drawerLayout) {
        return new ActionBarDrawerToggle(this,
                                        drawerLayout,
                                        null,
                         0,
                         0){

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                //getActionBar().setTitle(mTitle);
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
                //getActionBar().setTitle(mDrawerTitle);
            }
        };
    }

    private OnItemClickListener getListenerItemsNavigationDrawer(final Context context) {

        OnItemClickListener listener = new OnItemClickListener() {
            Intent intent = new Intent();
            DialogCreator dialogCreator = DialogCreator.getInstancia(context);
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {

                switch (position){
                    case 1:
                        intent = new Intent(context, CasoAMostrarActivity.class);
                        intent.putExtra(DatabasePAConstantes.CASO, DatabasePAConstantes.CASO_MEDIDAS_GENERALES);
                        startActivity(intent);
                    break;

                    case 2:
                        intent = new Intent(context, IndiceCasosActivity.class);
                        startActivity(intent);
                    break;

                    case 3:
                        intent = new Intent(context, CasoAMostrarActivity.class);
                        intent.putExtra(DatabasePAConstantes.CASO, DatabasePAConstantes.TRIAGE);
                        startActivity(intent);
                    break;

                    case 4:
                        intent = new Intent(context, EscalaActivity.class);
                        intent.putExtra(DatabasePAConstantes.ESCALA, DatabasePAConstantes.ESCALA_CONSCIENCIA);
                        startActivity(intent);
                    break;

                    case 5:
                        intent = new Intent(context, EscalaActivity.class);
                        intent.putExtra(DatabasePAConstantes.ESCALA, DatabasePAConstantes.ESCALA_DOLOR);
                        startActivity(intent);
                    break;

                    case 6:
                        AlertDialog dialogoNumerosEmergencia = dialogCreator.crearDialogoNumerosEmergencia();
                        dialogoNumerosEmergencia.show();
                    break;
                    case 7:
                        intent = new Intent(context, MapaHospitalesActivity.class);
                        //intent.putExtra(DatabasePAConstantes.ESCALA, DatabasePAConstantes.ESCALA_DOLOR);
                        startActivity(intent);
                    break;

                    case 8:
                        AlertDialog dialogoMarcoLegal = dialogCreator.crearDialogoMarcoLegal();
                        dialogoMarcoLegal.show();
                    break;

                    case 9:
                        AlertDialog dialogoCreditos = dialogCreator.crearDialogoCreditos();
                        dialogoCreditos.show();
                    break;

                    case 10:
                        intent = new Intent(context, TutorialYoutubeActivity.class);
                        //intent.putExtra(DatabasePAConstantes.ESCALA, DatabasePAConstantes.ESCALA_DOLOR);
                        startActivity(intent);
                    break;
                }
            }
        };

        return listener;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }


    public boolean onOptionsItemSelected(MenuItem item) {

        Intent intent;
        DialogCreator dialogCreator = DialogCreator.getInstancia(this);
        switch (item.getItemId()) {

            case android.R.id.home:
                this.drawerLayout.openDrawer(GravityCompat.START);  // OPEN DRAWER
                return true;
            case R.id.itemTamañoLetra:
                AlertDialog dialogoTamañoLetra = dialogCreator.crearDialogoTamañoLetra();
                dialogoTamañoLetra.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /*private ArrayList<Integer> obtenerIdsIconos(ArrayList<String> texts){//Obtener los ids de los iconos de cada caso
        ArrayList<Integer> images = new ArrayList<Integer>();

        for (int i = 0; i < texts.size(); i++){
            int identifier = getResources().getIdentifier(StringUtils.stripAccents(texts.get(i)), "drawable", this.getPackageName());
            images.add(identifier);
        }

        return images;
    }*/


    public boolean onQueryTextSubmit(String query) {
        this.listaCasos = Aplicacion.getInstancia(getApplicationContext()).getNombresCasos(query);
        //ArrayList<String> listaIconos = limpiarNombreListaCasos((ArrayList<String>) this.listaCasos.clone());
        this.iconosCasos = (ArrayList<Integer>) this.drawableManager.obtenerIdsIconosCasos((ArrayList<String>)listaCasos.clone());
        this.adapter = new ListaCasosCustomAdapter(this, this.listaCasos, this.iconosCasos);
        this.lvResultados.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
        this.simpleSearchView.setInputType(0);
        return false;
    }

    public boolean onQueryTextChange(String newText) {
        //this.listaCasos = new ArrayList<>();
        this.listaCasos = Aplicacion.getInstancia(getApplicationContext()).getNombresCasos(newText);
        Aplicacion.getInstancia(getApplicationContext()).getNombresCasos(newText);
        //ArrayList<String> listaIconos = limpiarNombreListaCasos((ArrayList<String>) this.listaCasos.clone());
        this.iconosCasos = (ArrayList<Integer>) this.drawableManager.obtenerIdsIconosCasos((ArrayList<String>)listaCasos.clone());
        this.adapter = new ListaCasosCustomAdapter(this, this.listaCasos, this.iconosCasos);
        this.lvResultados.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
        return false;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //String casoSeleccionado = this.lvResultados.getAdapter().getItem(i).toString();
        String casoSeleccionado = adapterView.getItemAtPosition(i).toString();
        Intent intent = new Intent(this, CasoAMostrarActivity.class);
        intent.putExtra(DatabasePAConstantes.CASO, casoSeleccionado);
        this.listaCasos = new ArrayList<>();
        this.iconosCasos = (ArrayList<Integer>) this.drawableManager.obtenerIdsIconosCasos(this.listaCasos);
        this.listaCasos.clear();
        this.iconosCasos.clear();
        this.adapter = new ListaCasosCustomAdapter(this, this.listaCasos, this.iconosCasos);
        //this.adapter = new ListaCasosCustomAdapter(this, null, null);
        this.lvResultados.setAdapter(this.adapter);
        this.adapter.notifyDataSetChanged();
        this.simpleSearchView.setQuery("",false);
        this.simpleSearchView.clearFocus();
        startActivity(intent);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.drawableManager = null;
    }
}

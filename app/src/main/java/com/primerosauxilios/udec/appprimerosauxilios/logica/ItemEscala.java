package com.primerosauxilios.udec.appprimerosauxilios.logica;

public class ItemEscala {

    private String criterio;
    private String descripcion;
    private int puntaje;

    public ItemEscala(){

    }

    public String getCriterio() {
        return criterio;
    }

    public void setCriterio(String criterio) {
        this.criterio = criterio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getPuntaje() {
        return puntaje;
    }

    public void setPuntaje(int puntaje) {
        this.puntaje = puntaje;
    }
}

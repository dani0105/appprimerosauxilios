package com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.ItemEscala;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.EscalaActivity;

import java.util.ArrayList;
import java.util.List;

public class ListaEscalaDolorCustomAdapter extends BaseAdapter implements CustomAdapter {

    private Context context;
    private List<ItemEscala> itemsEscala;
    private List<Integer> iconosPasos;

    public ListaEscalaDolorCustomAdapter(Context context,
                                         List<ItemEscala> itemsEscala,
                                         ArrayList<Integer> iconosPasos) {

        this.context = context;
        this.itemsEscala = itemsEscala;
        this.iconosPasos = iconosPasos;
    }

    @Override
    public int getCount() {
        return itemsEscala.size();
    }

    @Override
    public Object getItem(int position) {
        return itemsEscala.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //TODO AGREGAR LOS ICONOS
        LayoutInflater inflater = LayoutInflater.from(context);
        View row;
        row = inflater.inflate(
                R.layout.filalistaescaladolor_listview, parent, false);

        TextView tvRangoDolor = (TextView) row.findViewById(R.id.tvRangoDolor);
        TextView tvDescripcionDolor = (TextView) row.findViewById(R.id.tvDescripcionDolor);;
        ImageView ivDolor = (ImageView) row.findViewById(R.id.ivDolor);;

        SharedPreferences sharedPreferences =
                context.getSharedPreferences(context.getString(R.string.tamañoLetra),
                        context.MODE_PRIVATE);

        int tamañoLetra = sharedPreferences.getInt(context.getString(R.string.tamañoLetra), 15);
        tvRangoDolor.setTextSize(tamañoLetra);
        tvDescripcionDolor.setText(itemsEscala.get(position).getDescripcion());
        Typeface fontRalewayLight = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Light.ttf");

        tvRangoDolor.setTypeface(fontRalewayLight);
        tvDescripcionDolor.setTypeface(fontRalewayLight);

        switch(itemsEscala.get(position).getPuntaje()){
            case 0:
                //row.setBackgroundColor(context.getResources().getColor(R.color.yellowEscalaDolor));
                tvRangoDolor.setText("0 y 1");
                ivDolor.setImageResource(this.iconosPasos.get(0));
            break;

            case 2:
                //row.setBackgroundColor(context.getResources().getColor(R.color.grayEscalaDolor));
                tvRangoDolor.setText("2 y 3");
                ivDolor.setImageResource(this.iconosPasos.get(1));
            break;

            case 4:
                //row.setBackgroundColor(context.getResources().getColor(R.color.greenEscalaDolor));
                tvRangoDolor.setText("4 y 5");
                ivDolor.setImageResource(this.iconosPasos.get(2));
            break;

            case 6:
                //row.setBackgroundColor(context.getResources().getColor(R.color.aquaMarineEscalaDolor));
                tvRangoDolor.setText("6 y 7");
                ivDolor.setImageResource(this.iconosPasos.get(3));
            break;

            case 8:
                //row.setBackgroundColor(context.getResources().getColor(R.color.blueEscalaDolor));
                tvRangoDolor.setText("8 y 9");
                ivDolor.setImageResource(this.iconosPasos.get(4));
            break;

            case 10:
                //row.setBackgroundColor(context.getResources().getColor(R.color.redEscalaDolor));
                tvRangoDolor.setText("10");
                ivDolor.setImageResource(this.iconosPasos.get(5));
            break;

        }
        return row;
    }
}

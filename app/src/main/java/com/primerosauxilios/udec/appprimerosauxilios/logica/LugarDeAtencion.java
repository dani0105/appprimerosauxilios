package com.primerosauxilios.udec.appprimerosauxilios.logica;

public class LugarDeAtencion {

    private int id;
    private String nombre;
    private double latitud;
    private double longitud;
    private String direccion;
    private String telefono;

    public LugarDeAtencion() {
        this.id = 0;
        this.nombre = "";
        this.latitud = 0.0;
        this.longitud = 0.0;
        this.direccion = "";
        this.telefono = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
}

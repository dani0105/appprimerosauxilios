package com.primerosauxilios.udec.appprimerosauxilios.vista.activities;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.MenuItem;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Aplicacion;
import com.primerosauxilios.udec.appprimerosauxilios.logica.ItemEscala;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DatabasePAConstantes;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.ListaEscalaDolorCustomAdapter;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class EscalaActivity extends AppCompatActivity {

    private String nombreEscala;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        try{
            super.onCreate(savedInstanceState);
        }catch (NullPointerException e){
            return;
        }

        this.nombreEscala = getIntent().getStringExtra(DatabasePAConstantes.ESCALA);
        switch (this.nombreEscala){
            case DatabasePAConstantes.ESCALA_CONSCIENCIA:
                cargarEscalaConsciencia();
            break;
            case DatabasePAConstantes.ESCALA_DOLOR:
                cargarEscalaDolor();
            break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // ReHome
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void cargarEscalaDolor() {
        setContentView(R.layout.activity_caso_escala_dolor);
        List<ItemEscala> itemEscala = Aplicacion.getInstancia(getApplicationContext()).getEscala(DatabasePAConstantes.ESCALA_DOLOR);
        ListView lvEscalaDolor = (ListView) findViewById(R.id.lvEscalaDolor);
        //lvEscalaDolor.setDividerHeight(0);
        //lvEscalaDolor.setDivider(null);

        cargarListViewEscalaDolor(lvEscalaDolor, itemEscala);
        configurarToolbar();
        setTitle(getString(R.string.titleEscalaDolor));
    }

    private void cargarListViewEscalaDolor(ListView lvEscalaDolor, List<ItemEscala> itemsEscala) {
        ArrayList<Integer> iconosEscalaDolor = obtenerIdsIconosEscalaDolor();
        ListaEscalaDolorCustomAdapter adapter = new ListaEscalaDolorCustomAdapter(this, itemsEscala, iconosEscalaDolor);
        lvEscalaDolor.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    private ArrayList<Integer> obtenerIdsIconosEscalaDolor(){
        ArrayList<String> listaIconos = new ArrayList<String>(Arrays.asList(getResources().getStringArray(R.array.iconosEscalaDolor)));
        ArrayList<Integer> listaIdentificadores = new ArrayList<Integer>();

        for (String icono : listaIconos){
            int identificador  = getResources().getIdentifier(icono, "drawable", this.getPackageName());
            listaIdentificadores.add(identificador);
        }

        return listaIdentificadores;
        //return null;
    }

    private void cargarEscalaConsciencia() {
        setContentView(R.layout.activity_caso_escala_conciencia);

        List<ItemEscala> escalaConsciencia = Aplicacion.getInstancia(getApplicationContext()).getEscala(DatabasePAConstantes.ESCALA_CONSCIENCIA);
        TableLayout tabla = findViewById(R.id.tlItemsEscala);
        TextView tvCasoEscala = findViewById(R.id.tvCasoEscala);
        tvCasoEscala.setText(getResources().getString(R.string.strTvCasoEscalaConsciencia));
        //boolean bandera = true;
        for( ItemEscala item : escalaConsciencia) {
            TableRow tableRow = new TableRow(this);

            TextView tvCriterio = new TextView(this);
            TextView tvDescripcion = new TextView(this);
            TextView tvPuntaje = new TextView(this);

            TableRow.LayoutParams trParams = new TableRow.LayoutParams(0,
                    TableRow.LayoutParams.WRAP_CONTENT,
                    1);
            tvCriterio.setText(item.getCriterio());
            tvCriterio.setTextSize(15);
            tvCriterio.setGravity(Gravity.CENTER_HORIZONTAL);
            tvCriterio.setPadding(5, 0, 5, 0);
            tvCriterio.setLayoutParams(trParams);

            tvDescripcion.setText(item.getDescripcion());
            tvDescripcion.setTextSize(15);
            tvDescripcion.setGravity(Gravity.CENTER_HORIZONTAL);
            tvDescripcion.setPadding(5, 0, 5, 0);
            tvDescripcion.setLayoutParams(trParams);

            tvPuntaje.setText("" + item.getPuntaje());
            tvPuntaje.setTextSize(15);
            tvPuntaje.setGravity(Gravity.CENTER_HORIZONTAL);
            tvPuntaje.setPadding(5, 0, 5, 0);
            tvPuntaje.setLayoutParams(trParams);

            tableRow.setWeightSum(3);
            tableRow.addView(tvCriterio);
            tableRow.addView(tvDescripcion);
            tableRow.addView(tvPuntaje);
            tableRow.setPadding(0, 30, 0, 30);

            tabla.addView(tableRow);

            if (item.getCriterio().equalsIgnoreCase(getString(R.string.criterioEscalaConscienciaAperturaOcular))){
                tableRow.setBackgroundColor(getResources().getColor(R.color.redEscalaConsciencia));
                tvCriterio.setTextColor(Color.WHITE);
                tvDescripcion.setTextColor(Color.WHITE);
                tvPuntaje.setTextColor(Color.WHITE);
                //bandera = false;
            }
            else if(item.getCriterio().equalsIgnoreCase(getString(R.string.criterioEscalaConscienciaRespuestaVerbal))){
                tableRow.setBackgroundColor(getResources().getColor(R.color.blueEscalaConsciencia));
                tvCriterio.setTextColor(Color.WHITE);
                tvDescripcion.setTextColor(Color.WHITE);
                tvPuntaje.setTextColor(Color.WHITE);
            }
            else if(item.getCriterio().equalsIgnoreCase(getString(R.string.criterioEscalaConscienciaRespuestaMotora))){
                tableRow.setBackgroundColor(getResources().getColor(R.color.purpleEscalaConsciencia));
                tvCriterio.setTextColor(Color.WHITE);
                tvDescripcion.setTextColor(Color.WHITE);
                tvPuntaje.setTextColor(Color.WHITE);
            }




        }
        configurarToolbar();
        setTitle(DatabasePAConstantes.ESCALA_CONSCIENCIA);
    }

    private void configurarToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.primeros_auxilios_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
}

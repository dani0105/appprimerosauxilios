package com.primerosauxilios.udec.appprimerosauxilios.vista.activities;

import android.content.Context;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Toast;
import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Aplicacion;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Caso;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Triage;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DatabasePAConstantes;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.CustomAdapter;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.ListaCasosCustomAdapter;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.ListaPasosCustomAdapter;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.ListaTriagesCustomAdapter;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.dialogs.DialogCreator;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.extras.DrawableManager;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CasoAMostrarActivity extends AppCompatActivity{
    private static int unTiempo = 0;
    private SeekBar barraReproduccion;
    private ImageButton btnPlayPause;
    private Thread ActualizarTiempoAudio = new Hilo();
    private Caso caso;
    private Handler manejador;
    private String nombreCaso;
    private DrawableManager drawableManager;
    private List<String> listaPasos;
    private List<Integer> iconosPasos;
    private CustomAdapter adapter;
    private boolean reproduciendoAudio;
    private MediaPlayer reproductor;
    private int tiempoAdelante = 5000;
    private double tiempoComienzo = 0;
    private int tiempoDetras = 5000;
    private double tiempoFinal = 0;
    private ListView lvPasos;
    private Button btnTriage;
    private Context context;
    private Toolbar toolbar;

    protected void onCreate(Bundle savedInstanceState) {
        try{
            super.onCreate(savedInstanceState);
        }catch (NullPointerException e){
            return;
        }

        this.nombreCaso = getIntent().getStringExtra(DatabasePAConstantes.CASO);
        this.drawableManager = new DrawableManager(this);
        context = this;
        cargarCaso(this.nombreCaso);
        this.barraReproduccion = (SeekBar) findViewById(R.id.barraReproduccion);
        this.btnPlayPause = (ImageButton) findViewById(R.id.btnPlayPause);
        this.reproduciendoAudio = false;
        this.manejador = new Handler();

        //barraReproduccion.getThumb().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
    }

    private void configurarToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.primeros_auxilios_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // ReHome
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



    private OnSeekBarChangeListener getBarraReproduccionListener() {
        return new OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // TODO Auto-generated method stub
            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,boolean fromUser) {
                reproductor.seekTo(progress);
            }
        };
    }

    protected void cargarCaso(String nombreCaso) {

        String audio;

        switch (nombreCaso){

            case DatabasePAConstantes.TRIAGE:
                //setContentView(R.layout.activity_caso_triage);
                cargarTriage();
                audio = "triage";
                this.reproductor = MediaPlayer.create(this, getResources().getIdentifier(audio, "raw", getApplicationContext().getPackageName()));
                //barraReproduccion.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
                //setTitle(this.getResources().getString(R.string.triage));
                //this.reproduciendoAudio = false;
                //this.manejador = new Handler();
                //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            break;

            default:
                setContentView(R.layout.activity_caso);
                this.caso = Aplicacion.getInstancia(getApplicationContext()).getCaso(nombreCaso);
                this.lvPasos = (ListView) findViewById(R.id.lvPasos);
                this.lvPasos.setDividerHeight(0);
                this.lvPasos.setDivider(null);

                cargarListViewPasos();
                audio = this.caso.getAudioProcedimiento().toLowerCase();
                this.reproductor = MediaPlayer.create(this, getResources().getIdentifier(audio, "raw", getApplicationContext().getPackageName()));
                configurarToolbar();
                setTitle(this.caso.getNombre());
                cargarBotonTriage(this.caso.getIdTriage());
                //barraReproduccion.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.SRC_IN);
            break;
        }

    }

    private void setWeightView(View view, Integer width, Integer height, Integer weight){
        LayoutParams lp = new LayoutParams(width, height, weight);
        view.setLayoutParams(lp);
    }

    private void cargarBotonTriage(final Integer idTriage) {
        this.btnTriage = (Button) findViewById(R.id.btnTriage);
        if((idTriage == null) && (this.lvPasos != null)){
        //if(idTriage == null){
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.llActivityCaso);
            linearLayout.removeView((View) this.btnTriage.getParent());
            this.btnTriage.setVisibility(View.GONE);
            setWeightView(this.lvPasos, LayoutParams.MATCH_PARENT, 0, 8);
            return;
            //setWeightView((RelativeLayout)findViewById(R.id.rrBarraReproduccion), LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 2);
        }

        switch(idTriage){
            case 1:
                this.btnTriage.setBackgroundColor(this.getResources().getColor(R.color.redTriage));
                this.btnTriage.setText("TRIAGE: " + idTriage);
                this.btnTriage.setTextColor(Color.WHITE);
            break;

            case 2:
                this.btnTriage.setBackgroundColor(this.getResources().getColor(R.color.orangeTriage));
                this.btnTriage.setText("TRIAGE: " + idTriage);
                this.btnTriage.setTextColor(Color.WHITE);
            break;
            case 3:
                this.btnTriage.setBackgroundColor(this.getResources().getColor(R.color.yellowTriage));
                this.btnTriage.setText("TRIAGE: " + idTriage);
                this.btnTriage.setTextColor(Color.BLACK);
            break;

            case 4:
                this.btnTriage.setBackgroundColor(this.getResources().getColor(R.color.greenTriage));
                this.btnTriage.setText("TRIAGE: " + idTriage);
                this.btnTriage.setTextColor(Color.WHITE);
            break;

            case 5:
                this.btnTriage.setBackgroundColor(this.getResources().getColor(R.color.blueTriage));
                this.btnTriage.setText("TRIAGE " + idTriage);
                this.btnTriage.setTextColor(Color.WHITE);
            break;
        }

        this.context = this;

        this.btnTriage.setOnClickListener(new View.OnClickListener() {
            DialogCreator dialogCreator = DialogCreator.getInstancia(context);
            @Override
            public void onClick(View view) {
                AlertDialog dialogoTriage;
                switch (idTriage){
                    case 1:
                        dialogoTriage = dialogCreator.crearDialogoTriage(1);
                        dialogoTriage.show();
                    break;

                    case 2:
                        dialogoTriage = dialogCreator.crearDialogoTriage(2);
                        dialogoTriage.show();
                    break;

                    case 3:
                        dialogoTriage = dialogCreator.crearDialogoTriage(3);
                        dialogoTriage.show();
                    break;

                    case 4:
                        dialogoTriage = dialogCreator.crearDialogoTriage(4);
                        dialogoTriage.show();
                    break;

                    case 5:
                        dialogoTriage = dialogCreator.crearDialogoTriage(5);
                        dialogoTriage.show();
                    break;

                }
            }
        });
    }

    private void cargarTriage() {
        setContentView(R.layout.activity_caso_triage);
        List<Triage> triages = Aplicacion.getInstancia(getApplicationContext()).getTriages();
        this.lvPasos = (ListView) findViewById(R.id.lvPasos);
        this.lvPasos.setDividerHeight(0);
        this.lvPasos.setDivider(null);
        this.adapter = new ListaTriagesCustomAdapter(this, triages);
        this.lvPasos.setAdapter((ListaTriagesCustomAdapter) this.adapter);
        configurarToolbar();
        setTitle(getResources().getString(R.string.triage));
        ((ListaTriagesCustomAdapter)this.adapter).notifyDataSetChanged();
    }

    private void cargarListViewPasos() {
        this.listaPasos = obtenerPasosDeProcedimiento(this.caso.getProcedimiento());
        this.iconosPasos = this.drawableManager.obtenerIdsPasos();
        this.adapter = new ListaPasosCustomAdapter(this, this.listaPasos, this.iconosPasos);
        this.lvPasos.setAdapter((ListaPasosCustomAdapter)this.adapter);
        ((ListaPasosCustomAdapter)this.adapter).notifyDataSetChanged();
    }

    private List<String> obtenerPasosDeProcedimiento(String procedimiento) {

        ArrayList<String> pasos = new ArrayList<>(Arrays.asList(procedimiento.split("(\\<(b|p|h1)\\>){0,1}[\\s]{0,1}[\\d]{1,3}\\.[\\s]{0,3}")));

        pasos.remove("");
        return pasos;
    }

    private String limpiarNombreCaso(String nombreCaso){//metodo para convertir a minusculas y eliminar espacios, tildes y ñ
        //de la lista de casos
        return nombreCaso.toLowerCase().replaceAll("[(\\s{ASCII})]","").replaceAll("ñ", "n");
    }

    public void botonesAudio(View view) {
        switch (view.getId()) {
            case R.id.btnRetroceder:
                if (((int) this.tiempoComienzo) - this.tiempoDetras > 0) {
                    this.tiempoComienzo -= (double) this.tiempoDetras;
                    this.reproductor.seekTo((int) this.tiempoComienzo);
                    return;
                }
                return;
            case R.id.btnPlayPause:
                if (this.reproduciendoAudio) {
                    Toast.makeText(getApplicationContext(), "Pausando",Toast.LENGTH_SHORT).show();
                    this.reproductor.pause();
                    reproduciendoAudio = false;
                    btnPlayPause.setImageResource(R.drawable.ic_play_arrow_black_24dp);
                    return;
                }
                else {
                    Toast.makeText(getApplicationContext(), "Reproduciendo audio...", Toast.LENGTH_SHORT).show();
                    this.reproductor.start();
                    barraReproduccion.setMax(reproductor.getDuration());
                    this.tiempoFinal = (double) this.reproductor.getDuration();
                    this.tiempoComienzo = (double) this.reproductor.getCurrentPosition();
                    if (unTiempo == 0) {
                        unTiempo = 1;
                    }
                    reproduciendoAudio = true;
                }
                this.manejador.postDelayed(this.ActualizarTiempoAudio, 100);
                btnPlayPause.setImageResource(R.drawable.ic_pause_black_24dp);
                return;
            case R.id.btnAdelantar:
                if (((double) (this.tiempoAdelante + ((int) this.tiempoComienzo))) <= this.tiempoFinal) {
                    this.tiempoComienzo += (double) this.tiempoAdelante;
                    this.reproductor.seekTo((int) this.tiempoComienzo);
                    return;
                }
                return;
            default:
                return;
        }
    }


    protected void onStop() {
        super.onStop();
        if(!this.ActualizarTiempoAudio.isInterrupted() && this.reproductor != null) {
            this.reproductor.stop();
            this.ActualizarTiempoAudio.interrupt();
            this.ActualizarTiempoAudio = null;
        }
    }

    class Hilo extends Thread {
        private boolean ejecutarse = true;

        Hilo() {
        }

        public void run() {
            if (!this.ejecutarse) {
                CasoAMostrarActivity.this.tiempoComienzo = (double) CasoAMostrarActivity.this.reproductor.getCurrentPosition();
            }
            barraReproduccion.setProgress(reproductor.getCurrentPosition());

            barraReproduccion.setOnSeekBarChangeListener(getBarraReproduccionListener());
            CasoAMostrarActivity.this.manejador.postDelayed(this, 1000);
        }
    }
}

package com.primerosauxilios.udec.appprimerosauxilios.logica;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DAOPrimerosAuxilios;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DatabasePAConstantes;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DatabasePAConstantes.EntryCasos;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.PrimerosAuxiliosDBHelper;
import java.util.ArrayList;
import java.util.List;

public class Aplicacion {
    private static Aplicacion instancia;
    private Context context;
    private ArrayList<Caso> listaCasos = new ArrayList();
    private DAOPrimerosAuxilios daopa;

    private Aplicacion(Context context) {
        this.context = context;
        this.daopa = DAOPrimerosAuxilios.getInstancia(this.context);
    }

    public static Aplicacion getInstancia(Context context) {
        if (instancia == null) {
            instancia = new Aplicacion(context);
        }
        return instancia;
    }

    public ArrayList<Caso> getListaCasos() {
        return this.listaCasos;
    }

    public Caso getCaso(int id) {
        return null;
    }

    public Caso getCaso(String nombre) {
        return this.daopa.getCaso(nombre);
    }

    public List<Triage> getTriages() {
        return this.daopa.getTriages();
    }

    public ArrayList<String> getNombresCasos(String palabrasClave) {
        return this.daopa.getNombresCasos(palabrasClave);
    }

    public Triage getTriage(int idTriage) {
        return this.daopa.getTriage(idTriage);
    }

    public ArrayList<ItemEscala> getEscala(String escala){
        return this.daopa.getEscala(escala);
    }

    public String agregarNuevoCaso(String nombreCaso) {
        return null;
    }

    public String eliminarCaso(String nombreCaso) {
        return null;
    }


    public ArrayList<Normatividad> getMarcoLegal() {
        return this.daopa.getMarcoLegal();
    }

    public ArrayList<String> getNombresTodosCasos() {
        return this.daopa.getNombresTodosCasos();
    }


    public List<LugarDeAtencion> getAllLugaresDeAtencion() {
        return this.daopa.getAllLugaresDeAtencion();

    }
}

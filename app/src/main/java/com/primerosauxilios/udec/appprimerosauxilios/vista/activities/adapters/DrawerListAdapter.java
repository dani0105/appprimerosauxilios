package com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.extras.DrawerItem;

import java.util.List;

public class DrawerListAdapter extends ArrayAdapter implements CustomAdapter{

    private Context context;
    private Object[] items;
    public DrawerListAdapter(Context context, Object[] objects) {
        super(context, 0, objects);
        this.context = context;
        this.items = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater)parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.item_navigationdrawer, null);
        }


        ImageView iconItemNavigationDrawer = (ImageView) convertView.findViewById(R.id.ivItem_NavigationDrawer);
        TextView textItemNavigationDrawer = (TextView) convertView.findViewById(R.id.tvNombreItem_NavigationDrawer);
        DrawerItem item = (DrawerItem) getItem(position);
        iconItemNavigationDrawer.setImageResource(item.getIconId());
        textItemNavigationDrawer.setText(item.getName());
        textItemNavigationDrawer.setTextSize(20);
        Typeface fontRalewayLight = Typeface.createFromAsset(this.context.getAssets(), "fonts/Raleway-Light.ttf");
        textItemNavigationDrawer.setTypeface(fontRalewayLight);

        convertView.setPadding(10,70, 0, 70);
        return convertView;
    }
}

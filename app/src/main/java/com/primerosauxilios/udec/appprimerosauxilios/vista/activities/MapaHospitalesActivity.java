package com.primerosauxilios.udec.appprimerosauxilios.vista.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Aplicacion;
import com.primerosauxilios.udec.appprimerosauxilios.logica.LugarDeAtencion;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.SharedPreferencesManager;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.dialogs.DialogCreator;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.extras.CustomInfoWindowAdapter;
import java.text.DecimalFormat;
import java.util.List;

public class MapaHospitalesActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mapa;
    private Toolbar toolbar;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    private GoogleApiClient googleApiClient;
    private DialogCreator dialogCreator;
    private int rangoDeBusquedaEnKilometros;
    List<LugarDeAtencion> lugaresDeAtencion;
    private Location posicionActual;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mapa_hospitales);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        configurarToolbar();
        this.dialogCreator = DialogCreator.getInstancia(this);
    }

    private void configurarToolbar(){
        setSupportActionBar(this.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.primeros_auxilios_icon);
        actionBar.setDisplayHomeAsUpEnabled(true);
    }

    public void addMarker(LugarDeAtencion lugarDeAtencion, double distanciaDesdeLaUbicacionActual){

        MarkerOptions markerOptions = new MarkerOptions();

        LatLng latLng = new LatLng(lugarDeAtencion.getLatitud(), lugarDeAtencion.getLongitud());
        markerOptions.position(latLng);
        markerOptions.title(lugarDeAtencion.getNombre());
        markerOptions.snippet("Telefono: "+lugarDeAtencion.getTelefono()
                            + "\nDirección: " + lugarDeAtencion.getDireccion()
                            + "\nDistancia en linea recta: " + new DecimalFormat("#0.000").format(distanciaDesdeLaUbicacionActual)  + " km");
        //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_AZURE));
        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.hospital_map_icon_20));
        mapa.addMarker(markerOptions);
        //mapa.moveCamera(CameraUpdateFactory.newLatLng(p.getLatLng()));
        //mapa.animateCamera(CameraUpdateFactory.zoomTo(13));

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mapa = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) !=
                        PackageManager.PERMISSION_GRANTED) {

            mostrarUbicacionPorDefecto();
        }

        //this.posicionActual = locationManager.getLastKnownLocation(locationManager.getBestProvider(criteria, false));
        this.posicionActual = obtenerUltimaLocalizacion();
        if (this.posicionActual != null)
        {
            mapa.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(this.posicionActual.getLatitude(), this.posicionActual.getLongitude()), 13));

            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(this.posicionActual.getLatitude(), this.posicionActual.getLongitude()))
                    .zoom(17)
                    //.bearing(90)
                    //.tilt(40)
                    .build();

            mapa.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        }
        mapa.setOnMyLocationButtonClickListener(getMyLocationButtonClickListener());
        mapa.setOnMyLocationClickListener(getMyLocationClickListener());
        mapa.setOnCameraMoveListener(getOnCameraMoveListener());
        mapa.setMaxZoomPreference(20);
        habilitarMiUbicacionSiEstaPermitido();

        mapa.getUiSettings().setZoomControlsEnabled(true);
        mapa.setInfoWindowAdapter(new CustomInfoWindowAdapter(this));
        cargarListaPuntosDeAtencion();
        actualizarValorRangoBusqueda();

    }

    private Location obtenerUltimaLocalizacion() {
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Location bestLocation = null;
        for (String provider : providers) {
            @SuppressLint("MissingPermission")
            Location location = locationManager.getLastKnownLocation(provider);
            if (location == null) {
                continue;
            }
            if (bestLocation == null || location.getAccuracy() < bestLocation.getAccuracy()) {
                // Found best last known location: %s", l);
                bestLocation = location;
            }
        }
        return bestLocation;
        //Criteria criteria = new Criteria();


    }


    private GoogleMap.OnCameraMoveListener getOnCameraMoveListener() {
        return new GoogleMap.OnCameraMoveListener(){

            @Override
            public void onCameraMove() {
            }
        };
    }


    private GoogleMap.OnMyLocationClickListener getMyLocationClickListener() {

        return new GoogleMap.OnMyLocationClickListener() {
            @Override
            public void onMyLocationClick(@NonNull Location location) {

                //mapa.setMinZoomPreference(12);

                CircleOptions circleOptions = new CircleOptions();
                circleOptions.center(new LatLng(location.getLatitude(), location.getLongitude()));

                circleOptions.radius(200);
                circleOptions.fillColor(Color.RED);
                circleOptions.strokeWidth(6);

                mapa.addCircle(circleOptions);
            }
        };
    }

    private GoogleMap.OnMyLocationButtonClickListener getMyLocationButtonClickListener() {

        return new GoogleMap.OnMyLocationButtonClickListener() {
            @Override
            public boolean onMyLocationButtonClick() {
                mapa.setMinZoomPreference(15);
                return false;
            }
        };
    }


    private void habilitarMiUbicacionSiEstaPermitido() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
            != PackageManager.PERMISSION_GRANTED) {
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION},
                LOCATION_PERMISSION_REQUEST_CODE);
        } else if (mapa != null) {
            mapa.setMyLocationEnabled(true);
        }
    }

    private void mostrarUbicacionPorDefecto() {
        Toast.makeText(this, "Permiso de localización no concedido, " + "Mostrando localización por defecto",
                Toast.LENGTH_SHORT).show();
        LatLng redmond = new LatLng(47.6739881, -122.121512);
        mapa.moveCamera(CameraUpdateFactory.newLatLng(redmond));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        switch (requestCode) {
            case LOCATION_PERMISSION_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    habilitarMiUbicacionSiEstaPermitido();
                } else {
                    mostrarUbicacionPorDefecto();
                }
                return;
            }

        }
    }

    private boolean checkGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if(result != ConnectionResult.SUCCESS) {
            if(googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    protected synchronized void buildGoogleApiClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void cargarListaPuntosDeAtencion() {
        this.lugaresDeAtencion = Aplicacion.getInstancia(this).getAllLugaresDeAtencion();
    }

    public void cambiarRangoRedonda(View view) {
        AlertDialog dialogoRangoBusqueda = dialogCreator.crearDialogoRangoBusqueda();
        dialogoRangoBusqueda.show();
        dialogoRangoBusqueda.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                mapa.clear();
                actualizarValorRangoBusqueda();
            }
        });

    }

    private void actualizarValorRangoBusqueda() {
        SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(this);

        rangoDeBusquedaEnKilometros = sharedPreferencesManager.getRangoBusquedaAlmacenado();
        mostrarSitiosDeAtencionDeAcuerdoAlRango();

    }

    private void mostrarSitiosDeAtencionDeAcuerdoAlRango() {

        //Algoritmo para determinar si el punto de atencíon se encuentra dentro del rango especificado
        if(posicionActual != null) {
            double radioTierra = 6371; //En km
            for (LugarDeAtencion lugarDeAtencion : lugaresDeAtencion) {

                double dLat = Math.toRadians(lugarDeAtencion.getLatitud() - posicionActual.getLatitude());
                double dLng = Math.toRadians(lugarDeAtencion.getLongitud() - posicionActual.getLongitude());
                double sindLat = Math.sin(dLat / 2);
                double sindLng = Math.sin(dLng / 2);
                double va1 = Math.pow(sindLat, 2) + Math.pow(sindLng, 2)
                        * Math.cos(Math.toRadians(posicionActual.getLatitude())) * Math.cos(Math.toRadians(lugarDeAtencion.getLatitud()));
                double va2 = 2 * Math.atan2(Math.sqrt(va1), Math.sqrt(1 - va1));
                double distancia = radioTierra * va2;

                if (distancia <= rangoDeBusquedaEnKilometros)
                    addMarker(lugarDeAtencion, distancia);
            }
        }else{
            return;
        }
    }
}

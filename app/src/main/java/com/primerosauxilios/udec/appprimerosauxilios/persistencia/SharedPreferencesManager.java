package com.primerosauxilios.udec.appprimerosauxilios.persistencia;

import android.content.Context;
import android.content.SharedPreferences;

import com.primerosauxilios.udec.appprimerosauxilios.R;

public class SharedPreferencesManager {

    Context context;
    SharedPreferences sharedPreferences;

    public SharedPreferencesManager(Context context) {
        this.context = context;
        sharedPreferences =
                this.context.getSharedPreferences(context.getString(R.string.preferencias),
                        context.MODE_PRIVATE);
    }

    public int getTamanoLetraAlmacenado(){
        int tamanoLetra = this.sharedPreferences.getInt(context.getString(R.string.tamañoLetra), 15);
        return tamanoLetra;
    }

    public void setTamañoLetra(int nuevoTamañoLetra) {
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(this.context.getString(R.string.tamañoLetra), nuevoTamañoLetra);
        editor.commit();

    }

    public int getRangoBusquedaAlmacenado() {
        int rangoBusqueda = this.sharedPreferences.getInt(context.getString(R.string.rangoBusqueda), 5);
        return rangoBusqueda;
    }

    public void setRangoBusqueda(int nuevoRangoBusqueda) {
        final SharedPreferences.Editor editor = this.sharedPreferences.edit();
        editor.putInt(this.context.getString(R.string.rangoBusqueda), nuevoRangoBusqueda);
        editor.commit();
    }
}

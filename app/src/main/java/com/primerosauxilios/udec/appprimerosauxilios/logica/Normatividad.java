package com.primerosauxilios.udec.appprimerosauxilios.logica;

public class Normatividad {
    private int idNormativa;
    private String nombre;
    private String hyperlink;

    public Normatividad(){

    }

    public int getIdNormativa() {
        return idNormativa;
    }

    public void setIdNormativa(int idNormativa) {
        this.idNormativa = idNormativa;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getHyperlink() {
        return hyperlink;
    }

    public void setHyperlink(String hyperlink) {
        this.hyperlink = hyperlink;
    }
}

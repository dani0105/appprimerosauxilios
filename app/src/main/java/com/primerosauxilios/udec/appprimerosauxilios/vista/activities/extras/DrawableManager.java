package com.primerosauxilios.udec.appprimerosauxilios.vista.activities.extras;

import android.content.Context;

import com.primerosauxilios.udec.appprimerosauxilios.R;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.List;

public class DrawableManager {

    private Context context;

    public DrawableManager(Context context) {
        this.context = context;
    }

    public List<Integer> obtenerIdsIconosCasos(List<String> texts){//Obtener los ids de los iconos de cada caso
        ArrayList<Integer> images = new ArrayList<Integer>();
        texts = limpiarNombreListaCasos(texts);

        int identifier = 0;
        for (int i = 0; i < texts.size(); i++){
            /*try {
                identifier = R.drawable.class.getField(nombreImagen).getInt(null);
            } catch (Exception e){
                e.printStackTrace();
            }*/
            String nombreImagen = StringUtils.stripAccents(texts.get(i));
            identifier = context.getResources().getIdentifier(nombreImagen, "drawable", context.getPackageName());
            images.add(identifier);
        }
        return images;
    }

    public List<Integer> obtenerIdsPasos() {
        ArrayList<Integer> images = new ArrayList<Integer>();

        for (int i = 0; i < 14; i++){
            int identifier = context.getResources().getIdentifier( "a"+(i+1), "drawable", context.getPackageName());
            images.add(identifier);
        }

        return images;
    }

    private ArrayList<String> limpiarNombreListaCasos(List<String> listaCasos){//metodo para convertir a minusculas y eliminar espacios, tildes y ñ
        //de la lista de casos
        for (int i = 0; i < listaCasos.size(); i++){
            listaCasos.set(i, listaCasos.get(i).toLowerCase().replaceAll("[(\\s{ASCII})]","").replaceAll("ñ", "n"));
        }

        return (ArrayList<String>) listaCasos;
    }
}

package com.primerosauxilios.udec.appprimerosauxilios.vista.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

//import com.commit451.youtubeextractor.YouTubeExtractionResult;
//import com.commit451.youtubeextractor.YouTubeExtractor;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.YouTubePlayerView;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.player.listeners.YouTubePlayerInitListener;
import com.primerosauxilios.udec.appprimerosauxilios.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import at.huber.youtubeExtractor.YouTubeUriExtractor;
import at.huber.youtubeExtractor.YtFile;
//import retrofit2.Call;
//import retrofit2.Callback;
//import retrofit2.Response;

public class TutorialYoutubeActivity extends AppCompatActivity{

    //private static final String YOUTUBE_ID = "_I_D_8Z4sJE";
    private static final String YOUTUBE_ID = "z8Ffa8KpOYU";
    private YouTubePlayerView youTubeView;
    //private YouTubeExtractor extractor;
    //private Callback<YouTubeExtractionResult> extractionCallback;
    private ImageButton ibDescargar;
    private Context context;
    private ProgressDialog progressDialog;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial_youtube);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtubePlayerView);
        ibDescargar = (ImageButton) findViewById(R.id.ibDescargar);
        getLifecycle().addObserver(youTubeView);
        youTubeView.initialize(getYouTubePlayerInitListener(), true);
        //extractor = YouTubeExtractor.create();
        //extractionCallback = getExtractionCallBack();
        ibDescargar.setOnClickListener(getDescargarListener());
        configurarToolbar();
        context = this;
    }

    private void configurarToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.primeros_auxilios_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // ReHome
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public YouTubePlayerInitListener getYouTubePlayerInitListener() {
        return new YouTubePlayerInitListener() {

            @Override
            public void onInitSuccess(@NonNull final YouTubePlayer youTubePlayer) {
                youTubePlayer.addListener(
                        new AbstractYouTubePlayerListener() {
                            @Override
                            public void onReady() {
                                youTubePlayer.loadVideo(YOUTUBE_ID, 0);
                            }
                        });
            }

        };
    }

    public View.OnClickListener getDescargarListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //extractor.extract(YOUTUBE_ID).enqueue(extractionCallback);
                String link = "https://www.youtube.com/watch?v=" + YOUTUBE_ID;
                @SuppressLint("StaticFieldLeak") YouTubeUriExtractor ytEx = new YouTubeUriExtractor(context) {
                    @Override
                    public void onUrisAvailable(String videoID, String videoTitle, SparseArray<YtFile> ytFiles) {
                        if(ytFiles != null){
                            int itag = 135;
                            //This is the download URL
                            String downloadURL = ytFiles.get(itag).getUrl();
                            Log.e("download URL :", downloadURL);

                            //Now download it like a file
                            new RequestDownloadVideoStream().execute(downloadURL, videoID);

                        }
                    }
                };

                ytEx.execute(link);
            }
        };
    }

    private class RequestDownloadVideoStream extends AsyncTask<String, String, String>{

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(TutorialYoutubeActivity.this);
            progressDialog.setMessage("Descargando archivo. Por favor espere.");
            progressDialog.setIndeterminate(false);
            progressDialog.setMax(100);
            progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            InputStream is = null;
            URL u = null;
            int len1 = 0;
            int temp_progress = 0;
            int progress = 0;
            try {
                u = new URL(params[0]);
                is = u.openStream();
                URLConnection huc = (URLConnection) u.openConnection();
                huc.connect();
                int size = huc.getContentLength();

                if (huc != null) {
                    //String file_name = params[1] + ".mp4";
                    String file_name = "TutorialPrimerosAuxilios.mp4";
                    String storagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS)+"/YoutubeVideos";
                    File f = new File(storagePath);
                    if (!f.exists()) {
                        f.mkdir();
                    }

                    FileOutputStream fos = new FileOutputStream(f+"/"+file_name);
                    byte[] buffer = new byte[1024];
                    int total = 0;
                    if (is != null) {
                        while ((len1 = is.read(buffer)) != -1) {
                            total += len1;
                            // publishing the progress....
                            // After this onProgressUpdate will be called
                            progress = (int) ((total * 100) / size);
                            if(progress >= 0) {
                                temp_progress = progress;
                                publishProgress("" + progress);
                            }else
                                publishProgress("" + temp_progress+1);

                            fos.write(buffer, 0, len1);
                        }
                    }

                    if (fos != null) {
                        publishProgress("" + 100);
                        fos.close();
                    }
                }
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (is != null) {
                    try {
                        is.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            if (progressDialog.isShowing())
                progressDialog.dismiss();
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            progressDialog.setProgress(Integer.parseInt(values[0]));
        }

    }
    private void onError(Throwable t) {
        t.printStackTrace();
        Log.d("Error", t.getMessage());
        Toast.makeText(this, "Error al descargar", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        youTubeView.release();
    }
}

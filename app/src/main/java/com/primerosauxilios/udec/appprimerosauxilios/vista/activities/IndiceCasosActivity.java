package com.primerosauxilios.udec.appprimerosauxilios.vista.activities;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Aplicacion;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.DatabasePAConstantes;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.CustomAdapter;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.adapters.ListaCasosCustomAdapter;
import com.primerosauxilios.udec.appprimerosauxilios.vista.activities.extras.DrawableManager;

import java.util.ArrayList;
import java.util.List;

public class IndiceCasosActivity extends AppCompatActivity implements OnItemClickListener {

    private List<Integer> iconosCasos;
    private CustomAdapter adapter;
    private DrawableManager drawableManager;
    private ListView lvCasos;
    private Toolbar toolbar;
    private ArrayList<String> listaCasos;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_indice_de_casos);
        this.drawableManager = new DrawableManager(this);
        cargarIndiceDeCasos();
    }

    private void cargarIndiceDeCasos() {
        setContentView(R.layout.activity_indice_de_casos);
        ArrayList<String> listaTodosCasos = Aplicacion.getInstancia(getApplicationContext()).getNombresTodosCasos();
        this.iconosCasos = this.drawableManager.obtenerIdsIconosCasos((List<String>) listaTodosCasos.clone());
        this.lvCasos = (ListView) findViewById(R.id.lvListaCasos);
        this.lvCasos.setDividerHeight(0);
        this.lvCasos.setDivider(null);
        this.adapter = new ListaCasosCustomAdapter(this, listaTodosCasos, this.iconosCasos);
        this.lvCasos.setAdapter((ListaCasosCustomAdapter) this.adapter);
        this.lvCasos.setOnItemClickListener(this);
        configurarToolbar();
        setTitle(getResources().getString(R.string.listaCasos));
        ((ListaCasosCustomAdapter)this.adapter).notifyDataSetChanged();
    }

    private void configurarToolbar() {
        this.toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(this.toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setHomeAsUpIndicator(R.drawable.primeros_auxilios_icon);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // ReHome
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        //String casoSeleccionado = this.lvResultados.getAdapter().getItem(i).toString();
        String casoSeleccionado = adapterView.getItemAtPosition(i).toString();
        Intent intent = new Intent(this, CasoAMostrarActivity.class);
        intent.putExtra(DatabasePAConstantes.CASO, casoSeleccionado);
        this.listaCasos = new ArrayList<>();
        this.iconosCasos = (ArrayList<Integer>) this.drawableManager.obtenerIdsIconosCasos(this.listaCasos);
        //this.adapter = new ListaCasosCustomAdapter(this, this.li, this.iconosCasos);
        //this.lvCasos.setAdapter(this.adapter);
        //this.adapter.notifyDataSetChanged();
        startActivity(intent);
    }
}

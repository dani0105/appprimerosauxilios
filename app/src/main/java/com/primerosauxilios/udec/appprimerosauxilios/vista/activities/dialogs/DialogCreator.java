package com.primerosauxilios.udec.appprimerosauxilios.vista.activities.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.primerosauxilios.udec.appprimerosauxilios.R;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Aplicacion;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Caso;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Normatividad;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Triage;
import com.primerosauxilios.udec.appprimerosauxilios.persistencia.SharedPreferencesManager;

import java.util.ArrayList;

public class DialogCreator implements View.OnClickListener{
    private Context context;
    Typeface fontRalewayLight;

    private static DialogCreator instancia;

    public static DialogCreator getInstancia(Context context){
            instancia = new DialogCreator(context);
            return instancia;

            //TODo
    }

    private DialogCreator(Context context) {
        this.context = context;
        fontRalewayLight = Typeface.createFromAsset(this.context.getAssets(), "fonts/Raleway-Light.ttf");
    }

    public AlertDialog crearDialogoTamañoLetra(){
        //Inicializar el Alert Dialog

        AlertDialog.Builder dialogo = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle);
        final SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);

        LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.number_picker_dialog, null);
        dialogo.setTitle(R.string.strTituloDialogTamanoLetra);
        dialogo.setMessage(R.string.strMessageDialogTamanoLetra);
        dialogo.setView(dialogView);
        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.dialogo_tamaño_letra);
        numberPicker.setMaxValue(50);
        numberPicker.setMinValue(15);
        numberPicker.setWrapSelectorWheel(false);

        //Se recupera el valor almacenado de la letra, se le asigna al NumberPicker como valor por defecto
        //y se procede a codificar para almacenar el nuevo valor que se seleccione en el SharedPreferences

        int tamañoLetra = sharedPreferencesManager.getTamanoLetraAlmacenado();
        numberPicker.setValue(tamañoLetra);


        dialogo.setPositiveButton(R.string.strPositivoDialogTamanoLetra, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sharedPreferencesManager.setTamañoLetra(numberPicker.getValue());

            }
        });
        dialogo.setNegativeButton(R.string.strNegativoDialogTamanoLetra, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //editor.commit();
            }
        });
        AlertDialog alertDialog = dialogo.create();
        return alertDialog;
    }

    public AlertDialog crearDialogoNumerosEmergencia() {
        //Inicializar el Alert Dialog

        AlertDialog.Builder dialogo = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle);
        LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_dialog_numeros_emergencia, null);
        dialogo.setTitle("Números de emergencia");
        TextView tvTituloNumeroEmergencia = (TextView) dialogView.findViewById(R.id.tvTituloNumeroEmergencia);
        TextView tvNumeroEmergencia = (TextView) dialogView.findViewById(R.id.tvNumeroEmergencia);
        TextView tvTituloNumeroAmbulancia = (TextView) dialogView.findViewById(R.id.tvTituloNumeroAmbulancia);
        TextView tvNumeroAmbulancia = (TextView) dialogView.findViewById(R.id.tvNumeroAmbulancia);

        tvTituloNumeroEmergencia.setTypeface(fontRalewayLight);
        tvNumeroEmergencia.setTypeface(fontRalewayLight);
        tvTituloNumeroAmbulancia.setTypeface(fontRalewayLight);
        tvNumeroAmbulancia.setTypeface(fontRalewayLight);

        //formato al texto
        if (Build.VERSION.SDK_INT >= 24) {
            tvTituloNumeroEmergencia.setText(Html.fromHtml(context.getString(R.string.strTituloNumeroEmergencias), 0));
            tvNumeroEmergencia.setText(Html.fromHtml(context.getString(R.string.strNumeroEmergencias), 0));
            tvTituloNumeroAmbulancia.setText(Html.fromHtml(context.getString(R.string.strTituloNumeroAmbulancias), 0));
            tvNumeroAmbulancia.setText(Html.fromHtml(context.getString(R.string.strNumeroAmbulancias), 0));
        } else {
            tvTituloNumeroEmergencia.setText(Html.fromHtml(context.getString(R.string.strTituloNumeroEmergencias)));
            tvNumeroEmergencia.setText(Html.fromHtml(context.getString(R.string.strNumeroEmergencias)));
            tvTituloNumeroAmbulancia.setText(Html.fromHtml(context.getString(R.string.strTituloNumeroAmbulancias)));
            tvNumeroAmbulancia.setText(Html.fromHtml(context.getString(R.string.strNumeroAmbulancias)));
        }

        dialogo.setView(dialogView);

        dialogo.setPositiveButton(R.string.strPositivoDialogMarcoLegal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog alertDialog = dialogo.create();
        return alertDialog;
        //alertDialog.show();
    }

    public AlertDialog crearDialogoMarcoLegal(){
        //Inicializar el Alert Dialog

        AlertDialog.Builder dialogo = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle);
        final ArrayList<Normatividad> normatividades = Aplicacion.getInstancia((AppCompatActivity)context).
                                                getMarcoLegal();
        LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_dialog_marco_legal, null);
        dialogo.setTitle(R.string.titleDialogMarcoLegal);
        TextView tvDescripcionMarcoLegal = (TextView) dialogView.findViewById(R.id.tvDescripcionMarcoLegal);
        TextView tvNormativaUno = (TextView) dialogView.findViewById(R.id.tvNormativaUno);
        TextView tvNormativaDos = (TextView) dialogView.findViewById(R.id.tvNormativaDos);
        TextView tvNormativaTres = (TextView) dialogView.findViewById(R.id.tvNormativaTres);
        TextView tvNormativaCuatro = (TextView) dialogView.findViewById(R.id.tvNormativaCuatro);

        tvNormativaUno.setText(normatividades.get(0).getNombre());
        tvNormativaDos.setText(normatividades.get(1).getNombre());
        tvNormativaTres.setText(normatividades.get(2).getNombre());
        tvNormativaCuatro.setText(normatividades.get(3).getNombre());
        tvDescripcionMarcoLegal.setText(normatividades.get(4).getHyperlink());

        tvNormativaUno.setTypeface(fontRalewayLight);
        tvNormativaDos.setTypeface(fontRalewayLight);
        tvNormativaTres.setTypeface(fontRalewayLight);
        tvNormativaCuatro.setTypeface(fontRalewayLight);
        tvDescripcionMarcoLegal.setTypeface(fontRalewayLight);

        tvDescripcionMarcoLegal.setTypeface(null, Typeface.BOLD);

        tvNormativaUno.setPaintFlags(tvNormativaUno.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvNormativaUno.setTextColor(Color.BLUE);

        tvNormativaDos.setPaintFlags(tvNormativaDos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvNormativaDos.setTextColor(Color.BLUE);

        tvNormativaTres.setPaintFlags(tvNormativaTres.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvNormativaTres.setTextColor(Color.BLUE);

        tvNormativaCuatro.setPaintFlags(tvNormativaDos.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tvNormativaCuatro.setTextColor(Color.BLUE);


        tvNormativaUno.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(normatividades.get(0).getHyperlink()));
                context.startActivity(intent);
            }
        });

        tvNormativaDos.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(normatividades.get(1).getHyperlink()));
                context.startActivity(intent);
            }
        });

        tvNormativaTres.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(normatividades.get(2).getHyperlink()));
                context.startActivity(intent);
            }
        });

        tvNormativaCuatro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent= new Intent(Intent.ACTION_VIEW, Uri.parse(normatividades.get(3).getHyperlink()));
                context.startActivity(intent);
            }
        });

        dialogo.setView(dialogView);

        dialogo.setPositiveButton(R.string.strPositivoDialogMarcoLegal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog alertDialog = dialogo.create();
        //alertDialog.show();
        return alertDialog;
    }

    public AlertDialog crearDialogoCreditos(){
        //Inicializar el Alert Dialog

        AlertDialog.Builder dialogo = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle);
        Caso casoMarcoLegal = Aplicacion.getInstancia(context).getCaso("Creditos");
        LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_dialog, null);
        dialogo.setTitle(casoMarcoLegal.getNombre());
        TextView texto = (TextView) dialogView.findViewById(R.id.tvTextoDialog);
        texto.setTypeface(fontRalewayLight);

        //formato al texto
        if (Build.VERSION.SDK_INT >= 24) {
            texto.setText(Html.fromHtml(casoMarcoLegal.getProcedimiento(), 0));
        } else {
            texto.setText(Html.fromHtml(casoMarcoLegal.getProcedimiento()));
        }

        dialogo.setView(dialogView);

        dialogo.setPositiveButton(R.string.strPositivoDialogMarcoLegal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog alertDialog = dialogo.create();
        return alertDialog;
        //alertDialog.show();

    }

    public AlertDialog crearDialogoTriage(int idTriage) {
        AlertDialog.Builder dialogo = new AlertDialog.Builder((AppCompatActivity)context);
        Triage triage = Aplicacion.getInstancia(context).getTriage(idTriage);
        LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_dialog_triage, null);
        dialogo.setTitle(("TRIAGE #" + triage.getIdTriage() + ", " + triage.getNombre()).toUpperCase());
        TextView tvDialogDescripcionTriage = (TextView) dialogView.findViewById(R.id.tvDialogDescripcionTriage);
        tvDialogDescripcionTriage.setTypeface(fontRalewayLight);

        //formato al texto
        if (Build.VERSION.SDK_INT >= 24) {
             tvDialogDescripcionTriage.setText(Html.fromHtml(triage.getDescripcion(), 0));
        } else {
            tvDialogDescripcionTriage.setText(Html.fromHtml(triage.getDescripcion()));
        }

        dialogo.setView(dialogView);
        dialogo.setPositiveButton(R.string.strPositivoDialogMarcoLegal, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        AlertDialog alertDialog = dialogo.create();
        switch(triage.getColorTriage().toUpperCase()){
            case "RED":
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.redTriage);
            break;

            case "ORANGE":
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.orangeTriage);
            break;

            case "YELLOW":
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.yellowTriage);
            break;

            case "GREEN":
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.greenTriage);
            break;

            case "BLUE":
                alertDialog.getWindow().setBackgroundDrawableResource(R.color.blueTriage);
            break;
        }
        return alertDialog;
    }

    @Override
    public void onClick(View view) {

    }

    public AlertDialog crearDialogoRangoBusqueda() {
        //Inicializar el Alert Dialog

        AlertDialog.Builder dialogo = new AlertDialog.Builder(context, R.style.MyAlertDialogStyle);
        final SharedPreferencesManager sharedPreferencesManager = new SharedPreferencesManager(context);

        LayoutInflater inflater = ((AppCompatActivity)context).getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.layout_dialog_rango_busqueda, null);
        dialogo.setTitle(R.string.strTituloDialogRangoBusqueda);
        dialogo.setView(dialogView);
        final NumberPicker numberPicker = (NumberPicker) dialogView.findViewById(R.id.numberPickerRangoBusqueda);
        numberPicker.setMaxValue(25);
        numberPicker.setMinValue(1);
        numberPicker.setWrapSelectorWheel(false);

        //Se recupera el valor almacenado de la letra, se le asigna al NumberPicker como valor por defecto
        //y se procede a codificar para almacenar el nuevo valor que se seleccione en el SharedPreferences

        int rango = sharedPreferencesManager.getRangoBusquedaAlmacenado();
        numberPicker.setValue(rango);


        dialogo.setPositiveButton(R.string.strPositivoDialogTamanoLetra, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                sharedPreferencesManager.setRangoBusqueda(numberPicker.getValue());

            }
        });
        dialogo.setNegativeButton(R.string.strNegativoDialogTamanoLetra, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //editor.commit();
            }
        });
        AlertDialog alertDialog = dialogo.create();
        return alertDialog;
    }
}

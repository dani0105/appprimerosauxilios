package com.primerosauxilios.udec.appprimerosauxilios.persistencia;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.primerosauxilios.udec.appprimerosauxilios.logica.Caso;
import com.primerosauxilios.udec.appprimerosauxilios.logica.ItemEscala;
import com.primerosauxilios.udec.appprimerosauxilios.logica.LugarDeAtencion;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Normatividad;
import com.primerosauxilios.udec.appprimerosauxilios.logica.Triage;

import java.util.ArrayList;
import java.util.List;

public class DAOPrimerosAuxilios {

    private static DAOPrimerosAuxilios instancia;
    private PrimerosAuxiliosDBHelper admin;
    private SQLiteDatabase db;
    private Context context;

    private DAOPrimerosAuxilios(Context context){
        this.context = context;
        this.admin = new PrimerosAuxiliosDBHelper(this.context);
        inicializarBaseDatos();
    }

    public static DAOPrimerosAuxilios getInstancia(Context context){
        if(instancia != null){
            return instancia;
        }
        instancia = new DAOPrimerosAuxilios(context);
        return instancia;
    }

    public void inicializarBaseDatos() throws NullPointerException{
        try {
            this.db = this.admin.getWritableDatabase();
        }
        catch(NullPointerException e){
            e.printStackTrace();
        }
    }

    public Caso getCaso(String nombre) {

        Caso retorno = new Caso();
        Cursor caso = this.db.rawQuery("SELECT _id, " +
                "nombre, " +
                "procedimiento, " +
                "clavesBusqueda, " +
                "archivoAudio, " +
                "idTriage " +
                "FROM Casos " +
                "WHERE nombre " +
                "LIKE '%" + nombre + "%' ", null);
        caso.moveToFirst();
        retorno.setId(caso.getInt(0));
        retorno.setNombre(caso.getString(1));
        retorno.asignarProcedimiento(caso.getString(2));
        retorno.agregarPalabraClave(caso.getString(3));
        retorno.asignarAudioAProcedimiento(caso.getString(4));
        retorno.setIdTriage(caso.getInt(5) != 0 ? caso.getInt(5) : null );
        return retorno;
    }

    public List<Triage> getTriages() {

        List<Triage> listaTriages = new ArrayList<Triage>();
        Cursor triagesResult = this.db.rawQuery("SELECT * FROM Triage ", null);
        triagesResult.moveToFirst();
        do {
            Triage triage = new Triage();
            triage.setIdTriage(triagesResult.getInt(0));
            triage.setNombre(triagesResult.getString(1));
            triage.setTiempoAtencion(triagesResult.getString(2));
            triage.setDescripcion(triagesResult.getString(3));
            triage.setColorTriage(triagesResult.getString(4));

            listaTriages.add(triage);
        }while(triagesResult.moveToNext() != false);

        return listaTriages;
    }

    public ArrayList<String> getNombresCasos(String palabrasClave) {

        ArrayList<String> nombreCasos = new ArrayList();
        Cursor listaDeCasos = this.db.rawQuery("SELECT nombre " +
                "FROM Casos " +
                "WHERE clavesBusqueda " +
                "LIKE '%" + palabrasClave.toLowerCase() +
                "%'" + " ORDER BY nombre ASC", null);
        listaDeCasos.moveToFirst();
        while (!listaDeCasos.isAfterLast()) {
            nombreCasos.add(listaDeCasos.getString(listaDeCasos.getColumnIndex(DatabasePAConstantes.EntryCasos.NOMBRE)));
            listaDeCasos.moveToNext();
        }
        listaDeCasos.close();
        return nombreCasos;
    }

    public Triage getTriage(int idTriage) {

        Triage triage = new Triage();

        Cursor triageCursor = this.db.rawQuery("SELECT * " +
                "FROM Triage " +
                "WHERE idTriage == " + idTriage, null);

        triageCursor.moveToFirst();
        triage.setIdTriage(triageCursor.getInt(0));
        triage.setNombre(triageCursor.getString(1));
        triage.setTiempoAtencion(triageCursor.getString(2));
        triage.setDescripcion(triageCursor.getString(3));
        triage.setColorTriage(triageCursor.getString(4));

        triageCursor.close();

        return triage;
    }

    public ArrayList<ItemEscala> getEscala(String escala){
        int numeroEscala = 0;
        switch (escala){
            case DatabasePAConstantes.ESCALA_CONSCIENCIA:
                numeroEscala = 1;
                break;
            case DatabasePAConstantes.ESCALA_DOLOR:
                numeroEscala = 2;
                break;
        }
        ArrayList<ItemEscala> itemsEscala = new ArrayList<ItemEscala>();
        Cursor escalaConscienciaCursor = this.db.rawQuery("SELECT CriteriosEscala.descripcion AS 'Criterio', " +
                        "IndicadoresEscala.descripcion AS 'Descripción', " +
                        "IndicadoresEscala.puntaje AS 'Puntaje' " +
                        "FROM Escalas, CriteriosEscala, IndicadoresEscala " +
                        "WHERE IndicadoresEscala.idEscala = "+  numeroEscala + " " +
                        "AND CriteriosEscala.idEscala = "+  numeroEscala + " " +
                        "AND Escalas.idEscala = "+  numeroEscala + " " +
                        "AND IndicadoresEscala.idCriterio = CriteriosEscala.idCriterio " +
                        "ORDER BY CriteriosEscala.idCriterio, IndicadoresEscala.puntaje; "
                , null);

        escalaConscienciaCursor.moveToFirst();
        do {
            ItemEscala itemEscala = new ItemEscala();
            itemEscala.setCriterio(escalaConscienciaCursor.getString(0));
            itemEscala.setDescripcion(escalaConscienciaCursor.getString(1));
            itemEscala.setPuntaje(escalaConscienciaCursor.getInt(2));
            itemsEscala.add(itemEscala);
        }
        while (escalaConscienciaCursor.moveToNext());

        return itemsEscala;

    }

    public String agregarNuevoCaso(String nombreCaso) {
        return null;
    }

    public String eliminarCaso(String nombreCaso) {
        return null;
    }


    public ArrayList<Normatividad> getMarcoLegal() {

        ArrayList<Normatividad> normativas = new ArrayList<Normatividad>();

        Cursor listaDeNormativas = this.db.rawQuery("SELECT * FROM Normativas;", null);
        listaDeNormativas.moveToFirst();
        while (!listaDeNormativas.isAfterLast()) {
            Normatividad normativa = new Normatividad();
            normativa.setIdNormativa(listaDeNormativas.getInt(0));
            normativa.setNombre(listaDeNormativas.getString(1));
            normativa.setHyperlink(listaDeNormativas.getString(2));

            normativas.add(normativa);
            listaDeNormativas.moveToNext();
        }
        listaDeNormativas.close();
        return normativas;

    }

    public ArrayList<String> getNombresTodosCasos() {
        ArrayList<String> nombresTodosCasos = new ArrayList<String>();
        Cursor nombresCasosCursor = this.db.rawQuery("SELECT nombre FROM Casos ORDER BY nombre ASC;", null);

        nombresCasosCursor.moveToFirst();
        do {
            nombresTodosCasos.add(nombresCasosCursor.getString(0));
        }
        while (nombresCasosCursor.moveToNext());

        return nombresTodosCasos;
    }

    public void cerrarConexion(){
        instancia = null;
    }

    public List<LugarDeAtencion> getAllLugaresDeAtencion() {
        ArrayList<LugarDeAtencion> allLugaresDeAtencion = new ArrayList<>();
        Cursor allLugaresDeAtencionCursor = this.db.rawQuery("SELECT * FROM LugaresDeAtencion;", null);

        allLugaresDeAtencionCursor.moveToFirst();

        do {
            LugarDeAtencion lugarDeAtencion = new LugarDeAtencion();
            lugarDeAtencion.setId(allLugaresDeAtencionCursor.getInt(0));
            lugarDeAtencion.setNombre(allLugaresDeAtencionCursor.getString(1));
            lugarDeAtencion.setLatitud(allLugaresDeAtencionCursor.getDouble(2));
            lugarDeAtencion.setLongitud(allLugaresDeAtencionCursor.getDouble(3));
            lugarDeAtencion.setDireccion(allLugaresDeAtencionCursor.getString(4));
            lugarDeAtencion.setTelefono(allLugaresDeAtencionCursor.getString(5));
            allLugaresDeAtencion.add(lugarDeAtencion);
        }
        while (allLugaresDeAtencionCursor.moveToNext());

        return allLugaresDeAtencion;
    }
}

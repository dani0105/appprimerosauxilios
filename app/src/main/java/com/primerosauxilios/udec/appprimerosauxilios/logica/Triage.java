package com.primerosauxilios.udec.appprimerosauxilios.logica;

public class Triage {
    private int idTriage;
    private String nombre;
    private String tiempoAtencion;
    private String descripcion;
    private String colorTriage;

    public Triage(){

    }

    public int getIdTriage() {
        return idTriage;
    }

    public void setIdTriage(int idTriage) {
        this.idTriage = idTriage;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTiempoAtencion() {
        return tiempoAtencion;
    }

    public void setTiempoAtencion(String tiempoAtencion) {
        this.tiempoAtencion = tiempoAtencion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getColorTriage() {
        return colorTriage;
    }

    public void setColorTriage(String colorTriage) {
        this.colorTriage = colorTriage;
    }
}
